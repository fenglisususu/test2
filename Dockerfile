FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
EXPOSE 20000
COPY . /app
ENV ASPNETCORE_URLS=http://+:20000
ENTRYPOINT ["dotnet", "api.dll"]